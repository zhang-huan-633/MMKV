## MMKV单元测试用例

该测试用例基于OpenHarmony系统下，全量进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|version|pass|
|getRootDir|pass|
|pageSize|pass|
|getDefaultMMKV|pass|
|totalSize|pass|
|encode|pass|
|decodeString|pass|
|decodeBool|pass|
|decodeNumber|pass|
|decodeSet|pass|
|containsKey|pass|
|getCryptKey|pass|
|getMMapID|pass|
|removeValueForKey|pass|
|removeValuesForKeys|pass|
|clearAll|pass|
|count|pass|
|isFileValid|pass|
|reCryptKey|pass|
|backupOneToDirectory|pass|
|backupAllToDirectory|pass|
|restoreOneMMKVFromDirectory|pass|
|restoreAllFromDirectory|pass|
|initialize|pass|
|getBackedUpMMKVWithID|pass|
|encodeSerialize|pass|
|decodeSerialize|pass|
|encodeString|pass|
|encodeSet|pass|
|encodeBool|pass|
|encodeNumber|pass|
|getAllKeys|pass|
|clearMemoryCache|pass|
|actualSize|pass|
|getHandle|pass|
|close|pass|
|trim|pass|
|checkContentChangedByOuterProcess|pass|
|setLogLevel|pass|
|checkReSetCryptKey|pass|
|simpleLog|pass|
|preferencesToMMKV|pass|
|LogUtil.d|pass|
|LogUtil.i|pass|
|LogUtil_e|pass|
|isEnd|pass|
|close|pass|